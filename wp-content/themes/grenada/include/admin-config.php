<?php
/**
 * Grenada Theme Config File
 */

if ( ! class_exists( 'grenada_options_config' ) ) {

    class grenada_options_config {

        public $args = array();
        public $sections = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if ( ! class_exists( 'ReduxFramework' ) ) {
                return;
            }

            if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                $this->initSettings();
            } else {
                add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                return;
            }

            $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
        }

        /**
         * This is a test function that will let you see when the compiler hook occurs.
         * It only runs if a field    set with compiler=>true is changed.
         * */
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";

        }

        /**
         * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
         * Simply include this function in the child themes functions.php file.
         * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
         * so you must use get_template_directory_uri() if you want to use any of the built in icons
         * */
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'grenada'),
                'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'grenada'),
                'icon'   => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**
         * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
         * */
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**
         * Filter hook for filtering the default value of any given field. Very useful in development mode.
         * */
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        public function setSections() {

            /**
             * Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            ob_start();

            $ct          = wp_get_theme();
            $this->theme = $ct;
            $item_name   = $this->theme->get( 'Name' );
            $tags        = $this->theme->Tags;
            $screenshot  = $this->theme->get_screenshot();
            $class       = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf( esc_html__( 'Customize &#8220;%s&#8221;', 'grenada'), $this->theme->display( 'Name' ) );

            ?>
            <div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
                <?php if ( $screenshot ) : ?>
                    <?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize"
                           title="<?php echo esc_attr( $customize_title ); ?>">
                            <img src="<?php echo esc_url( $screenshot ); ?>"
                                 alt="<?php esc_attr_e( 'Current theme preview', 'grenada'); ?>"/>
                        </a>
                    <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>"
                         alt="<?php esc_attr_e( 'Current theme preview', 'grenada'); ?>"/>
                <?php endif; ?>

                <h4><?php echo wp_kses_post( $this->theme->display( 'Name' ) ); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf( esc_html__( 'By %s', 'grenada'), $this->theme->display( 'Author' ) ); ?></li>
                        <li><?php printf( esc_html__( 'Version %s', 'grenada'), $this->theme->display( 'Version' ) ); ?></li>
                        <li><?php echo '<strong>' . esc_html__( 'Tags', 'grenada') . ':</strong> '; ?><?php printf( $this->theme->display( 'Tags' ) ); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo wp_kses_post( $this->theme->display( 'Description' ) ); ?></p>
                    <?php
                    if ( $this->theme->parent() ) {
                        printf( ' <p class="howto">' . esc_html__( 'This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'grenada') . '</p>', esc_html__( 'http://codex.wordpress.org/Child_Themes', 'grenada'), $this->theme->parent()->display( 'Name' ) );
                    }
                    ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            // ACTUAL DECLARATION OF SECTIONS
            $this->sections[] = array(
                'title'     => esc_html__('General Settings', 'grenada'),
                'desc'      => esc_html__('General theme settings.', 'grenada'),
                'icon'      => 'el-icon-wrench',
                'fields'    => array(

					array(
                        'id'        => 'clapat_grenada_enable_ajax',
                        'type'      => 'switch',
                        'title'     => esc_html__('Load Pages With Ajax', 'grenada'),
						'desc'      => esc_html__('When navigate loads the target content without reloading the current page.', 'grenada'),
                        'default'   => false,
						'on'        => esc_html__('Yes', 'grenada'),
                        'off'       => esc_html__('No', 'grenada'),
                    ),
					
					array(
                        'id'        => 'clapat_grenada_enable_smooth_scrolling',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Smooth Scrolling', 'grenada'),
                        'default'   => false,
                        'on'        => esc_html__('Yes', 'grenada'),
                        'off'       => esc_html__('No', 'grenada'),
                    ),

					array(
                        'id'        => 'clapat_grenada_enable_preloader',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Page Preloader', 'grenada'),
                        'subtitle'  => esc_html__('Enable preloader mask while the page is loading.', 'grenada'),
                        'default'   => true,
						'on'        => esc_html__('Yes', 'grenada'),
                        'off'       => esc_html__('No', 'grenada'),
                    ),
					
					array(
                        'id'        => 'clapat_grenada_enable_fullscreen_menu',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Fullscreen menu on desktop', 'grenada'),
                        'default'   => false,
						'on'        => esc_html__('Yes', 'grenada'),
                        'off'       => esc_html__('No', 'grenada'),
                    ),
					
					array(
                        'id'        => 'clapat_grenada_enable_back_to_top',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Back To Top Button', 'grenada'),
                        'default'   => false,
						'on'        => esc_html__('Yes', 'grenada'),
                        'off'       => esc_html__('No', 'grenada'),
                    ),

					array(
                        'id'            => 'clapat_grenada_space_head',
                        'type'          => 'textarea',
                        'title'         => wp_kses_post( esc_html__('Space before &lt;/head&gt;', 'grenada') ),
                        'subtitle'      => wp_kses_post( esc_html__('Add code which is inserted before the &lt;/head&gt; tag.', 'grenada') ),
                        'default'       => '',
                    ),

                ),
            );

            $this->sections[] = array(
                'title'     => esc_html__('Header Options', 'grenada'),
                'desc'      => esc_html__('Header Options. Select the logo displayed in the header and settings related with the menu.', 'grenada'),
                'icon'      => 'el-icon-lines',
                'fields'    => array(

                    array(
                        'id'        => 'clapat_grenada_logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Header Logo', 'grenada'),
                        'desc'      => '',
                        'subtitle'  => esc_html__('Upload your logo to be displayed at the left side of the header menu.', 'grenada'),
                        'default'   => array('url' => get_template_directory_uri() . '/images/logo.png'),
                    ),

					array(
                        'id'        => 'clapat_grenada_logo_light',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Header Logo Light', 'grenada'),
                        'desc'      => '',
                        'subtitle'  => esc_html__('Light logo displayed on dark backgrounds.', 'grenada'),
                        'default'   => array('url' => get_template_directory_uri() . '/images/logo-white.png'),
                    ),

                ),

            );

			global $grenada_social_links;
			$footer_fields = array();
			$social_network_ids = array_keys( $grenada_social_links );

			$footer_fields[] = array(
											'id'            => 'clapat_grenada_footer_copyright_prefix',
											'type'          => 'text',
											'title'         => esc_html__('Copyright prefix', 'grenada'),
											'subtitle'      => esc_html__('Text preceding the copyright info.', 'grenada'),
											'default'       => esc_html__('Copyright', 'grenada'),
										);
										
			$footer_fields[] = array(
											'id'            => 'clapat_grenada_footer_copyright',
											'type'          => 'text',
											'title'         => esc_html__('Copyright text', 'grenada'),
											'subtitle'      => esc_html__('This is the copyright text displayed in the footer of the overlay menu.', 'grenada'),
											'default'       => wp_kses_post( __('<p>2018 &copy; <a target="_blank" href="https://www.clapat.com/themes/grenada/">Grenada Theme</a>.</p>', 'grenada') ),
										);
							 
			$footer_fields[] = array(
      								'id'            => 'clapat_grenada_footer_social_links_prefix',
      								'type'          => 'text',
      								'title'         => esc_html__('Social Links Prefix Text', 'grenada'),
      								'subtitle'      => esc_html__('Text preceding the social links.', 'grenada'),
      								'default'       => wp_kses_post( __('Follow us ', 'grenada') ),
      						 );

			for( $idx = 1; $idx <= GRENADA_MAX_SOCIAL_LINKS; $idx++ ){

				$footer_fields[] =	array(
											'id'        => 'clapat_grenada_footer_social_' . $idx,
											'type'      => 'select',
											'title'     => esc_html__('Social Network Name ', 'grenada' ) . $idx,
											'options'   => $grenada_social_links,
											'default'   => 'facebook'
										);
				$footer_fields[] = array(
											'id'        => 'clapat_grenada_footer_social_url_' . $idx,
											'type'      => 'text',
											'title'     => esc_html__('Social Link URL ', 'grenada' ) . $idx,
											'default'   => '',
										);
			}

		$this->sections[] = array(
			'title'     => esc_html__('Footer Options', 'grenada'),
			'desc'      => esc_html__('Settings concerning the footer such as social links and copyright text.', 'grenada'),
			'icon'      => 'el-icon-check-empty',
			'fields'    => $footer_fields
		);

		$this->sections[] = array(
                'title'     => esc_html__('Portfolio Options', 'grenada'),
                'desc'      => esc_html__('Settings concerning the portfolio section or portfolio pages.', 'grenada'),
                'icon'      => 'el-icon-folder-open',
                'fields'    => array(

                    array(
                        'id'        => 'clapat_grenada_portfolio_custom_slug',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom Slug', 'grenada'),
                        'desc'      => esc_html__('If you want your portfolio post type to have a custom slug in the url, please enter it here. You will still have to refresh your permalinks after saving this! This is done by going to Settings > Permalinks and clicking save.', 'grenada'),
                        'default'   => '',
                    ),

 					array(
                        'id'        => 'clapat_grenada_portfolio_next_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Next Project Caption', 'grenada'),
                        'subtitle'  => esc_html__('Caption of the next project in portfolio navigation.', 'grenada'),
                        'default'   => esc_html__('next', 'grenada'),
                    ),
					
					array(
                        'id'        => 'clapat_grenada_portfolio_case_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('View Project Caption', 'grenada'),
                        'subtitle'  => esc_html__('The caption the link to project page displayed in portfolio page templates when you hover the portfolio item thumbnail.', 'grenada'),
                        'default'   => esc_html__('View Case', 'grenada'),
                    ),
					
					array(
                        'id'        => 'clapat_grenada_portfolio_filter_all_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('All Category Caption', 'grenada'),
                        'subtitle'  => esc_html__('The caption the All category displaying all portfolio items in portfolio page templates.', 'grenada'),
                        'default'   => esc_html__('All', 'grenada'),
                    ),
					
					array(
						'id'        => 'clapat_grenada_portfolio_navigation_order',
						'type'      => 'select',
						'title'     => esc_html__('Portfolio Items Navigation Order', 'grenada'),
						'options'   => array( 'forward' => 'Forward in time (next item is newer or loops to the oldest if current item is the newest)',
											  'backward' => 'Backward in time (next item is older or loops to the newest if current item is the oldest)'  ),
						'default'   => 'forward'
					)

                ),
            );

            $this->sections[] = array(
                'title'     => esc_html__('Blog Options', 'grenada'),
                'desc'      => esc_html__('Settings concerning the blog section or blog pages.', 'grenada'),
                'icon'      => 'el-icon-bold',
                'fields'    => array(

                    array(
                        'id'        => 'clapat_grenada_blog_posted_by_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Posted By Caption', 'grenada'),
                        'subtitle'  => esc_html__('The caption displayed before post\'s author name', 'grenada'),
                        'default'   => esc_html__('Posted By', 'grenada')
                    ),

					array(
                        'id'        => 'clapat_grenada_blog_next_post_before',
                        'type'      => 'text',
                        'title'     => esc_html__('Before next post link', 'grenada'),
                        'desc'      => esc_html__('Caption displayed before the button linking to the next single blog post page.', 'grenada'),
                        'default'   => esc_html__('Read Also', 'grenada'),
                    ),
					
                    array(
                        'id'        => 'clapat_grenada_blog_next_post_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Next Post Caption', 'grenada'),
                        'desc'      => esc_html__('Caption of the button linking to the next single blog post page.', 'grenada'),
                        'default'   => esc_html__('Next', 'grenada'),
                    ),

					array(
                        'id'        => 'clapat_grenada_blog_default_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Default Posts Page Title', 'grenada'),
                        'desc'      => esc_html__('Title of the default blog posts page. The default blog posts page is the page displaying blog posts when there is no front page set in Settings -> Reading.', 'grenada'),
                        'default'   => esc_html__('Blog', 'grenada'),
                    ),
                )
            );

			$this->sections[] = array(
                'title'     => esc_html__('Typography', 'grenada'),
                'desc'      => esc_html__('Font options to render the typography style of your website.', 'grenada'),
                'icon'      => 'el-icon-fontsize',
                'fields'    => array(

					array(
                        'id'        => 'clapat_grenada_body_font',
                        'type'      => 'typography',
                        'output'    => array('html, body, p, .comment-head, .form-submit input[type="submit"], .post-password-form input[type="submit"], input[type="text"], input[type="search"], textarea'),
                        'title'     => esc_html__('Body Font Options', 'grenada'),
                        'subtitle'  => esc_html__('Select font options for the body', 'grenada'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => true,
                        'font-weight' => true,
						'color' => false,
						'line-height' => false
                    ),

                    array(
                        'id'        => 'clapat_grenada_heading_font',
                        'type'      => 'typography',
                        'output'    => array('h1, h2, h3, h4, h5, h6, .flexnav li a, .copyright-text, .socials-text, .button-text, .clapat-button'),
                        'title'     => esc_html__('Headings Font Options', 'grenada'),
                        'subtitle'  => esc_html__('Select font options for headings.', 'grenada'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => false,
						'font-size'	=> false,
						'line-height' => false,
                        'font-weight' => false,
						'font-style' => false,
						'all_styles' => true,
						'color' => false,
					),
				)
			);

            $this->sections[] = array(
                'title'     => esc_html__('Map Options', 'grenada'),
                'desc'      => esc_html__('Settings concerning the map section.', 'grenada'),
                'icon'      => 'el-icon-map-marker',
                'fields'    => array(

					array(
                        'id'        => 'clapat_grenada_map_marker',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Map Marker', 'grenada'),
                        'subtitle'  => esc_html__('Please choose an image file for the marker.', 'grenada'),
                        'default'   => array('url' => get_template_directory_uri() . '/images/marker.png'),
                    ),
					array(
                        'id'        => 'clapat_grenada_map_address',
                        'type'      => 'text',
                        'title'     => esc_html__('Google Map Address', 'grenada'),
                        'subtitle'  => esc_html__('Example: 775 New York Ave, Brooklyn, Kings, New York 11203. Or you can enter latitude and longitude for greater precision. Example: 41.40338, 2.17403 (in decimal degrees - DDD)', 'grenada'),
                        'default'   => esc_html__('775 New York Ave, Brooklyn, Kings, New York 11203', 'grenada'),
                    ),
					array(
                        'id'        => 'clapat_grenada_map_zoom',
                        'type'      => 'text',
                        'title'     => esc_html__('Map Zoom Level', 'grenada'),
                        'subtitle'  => esc_html__('Higher number will be more zoomed in.', 'grenada'),
                        'default'   => '16',
                    ),
                    array(
                        'id'        => 'clapat_grenada_map_company_name',
                        'type'      => 'text',
                        'title'     => esc_html__('Pop-up marker title', 'grenada'),
                        'default'   => esc_html__('grenada', 'grenada'),
                    ),
                    array(
                        'id'        => 'clapat_grenada_map_company_info',
                        'type'      => 'text',
                        'title'     => esc_html__('Pop-up marker text', 'grenada'),
                        'default'   => esc_html__('Here we are. Come to drink a coffee!', 'grenada'),
                    ),
                    array(
                        'id'        => 'clapat_grenada_map_type',
                        'type'      => 'switch',
                        'title'     => esc_html__('Map type', 'grenada'),
                        'subtitle'  => esc_html__('Set the map type as road map or satellite.', 'grenada'),
                        'on'        => esc_html__('Road', 'grenada'),
                        'off'       => esc_html__('Satellite', 'grenada'),
                        'default'   => true,
                    ),
					array(
                        'id'        => 'clapat_grenada_map_api_key',
                        'type'      => 'text',
                        'title'     => esc_html__('Google Maps API Key', 'grenada'),
						'subtitle'  => esc_html__('Without it, the map may not be displayed. If you have an api key paste it here. More information on how to obtain a google maps api key: https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key', 'grenada'),
                        'default'   => '',
                    ),
                )
            );

            $this->sections[] = array(
                'title'     => esc_html__('Error Page Options', 'grenada'),
                'desc'      => esc_html__('Settings concerning the "error not found page (404)" section.', 'grenada'),
                'icon'      => 'el-icon-error-alt',
                'fields'    => array(

                    array(
                        'id'        => 'clapat_grenada_error_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Error Page Title', 'grenada'),
                        'default'   => esc_html__('404', 'grenada'),
                    ),
					array(
                        'id'        => 'clapat_grenada_error_info',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Error Page Info text', 'grenada'),
                        'validate'  => 'html',
                        'default'   => esc_html__('The page you are looking for could not be found.', 'grenada'),
                    ),
                    array(
                        'id'        => 'clapat_grenada_error_back_button',
                        'type'      => 'text',
                        'title'     => esc_html__('Back Button Caption', 'grenada'),
                        'default'   => esc_html__('Home Page', 'grenada'),
                    ),

					array(
                        'id'        => 'clapat_grenada_error_back_button_url',
                        'type'      => 'text',
                        'title'     => esc_html__('Back Button URL', 'grenada'),
                        'default'   => esc_url( get_home_url() ),
                    ),

                )
            );

            $this->sections[] = array(
                'title'     => esc_html__('Import / Export', 'grenada'),
                'desc'      => esc_html__('Import and Export your settings from file, text or URL.', 'grenada'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => esc_html__('Import Export', 'grenada'),
                        'subtitle'      => esc_html__('Save and restore your theme options', 'grenada'),
                        'full_width'    => false,
                    ),
                ),
            );

        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'      => 'redux-help-tab-1',
                'title'   => esc_html__( 'Theme Information 1', 'grenada'),
                'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'grenada')
            );

            $this->args['help_tabs'][] = array(
                'id'      => 'redux-help-tab-2',
                'title'   => esc_html__( 'Theme Information 2', 'grenada'),
                'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'grenada')
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'grenada');
        }

        /**
         * All the possible arguments for Redux.
         * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'             => GRENADA_THEME_OPTIONS,
                // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'         => $theme->get( 'Name' ),
                // Name that appears at the top of your panel
                'display_version'      => $theme->get( 'Version' ),
                // Version that appears at the top of your panel
                'menu_type'            => 'submenu',
                //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'       => true,
                // Show the sections below the admin menu item or not
                'menu_title'        => esc_html__('Theme Options', 'grenada'),
                'page_title'        => $theme->get('Name') . esc_html__(' Theme Options', 'grenada'),
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key'       => 'AIzaSyCo4U6ficbcj__7_LC1YpFDxtmTJ-pzoQ0',
                // Set it you want google fonts to update weekly. A google_api_key value is required.
                'google_update_weekly' => false,
                // Must be defined to add google fonts to the typography module
                'async_typography'     => false,
                // Use a asynchronous font on the front end or font string
                //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                'admin_bar'            => true,
                // Show the panel pages on the admin bar
                'admin_bar_icon'     => 'dashicons-portfolio',
                // Choose an icon for the admin bar menu
                'admin_bar_priority' => 50,
                // Choose an priority for the admin bar menu
                'global_variable'      => 'grenada_theme_options',
                // Set a different name for your global variable other than the opt_name
                'dev_mode'             => false,
                // Show the time the page took to load, etc
                'update_notice'        => true,
                // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
                'customizer'           => true,
                // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                // OPTIONAL -> Give you extra features
                'page_priority'        => null,
                // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'          => 'themes.php',
                // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'     => 'manage_options',
                // Permissions needed to access the options panel.
                'menu_icon'            => '',
                // Specify a custom URL to an icon
                'last_tab'             => '',
                // Force your panel to always open to a specific tab (by id)
                'page_icon'            => 'icon-themes',
                // Icon displayed in the admin panel next to your menu_title
                'page_slug'            => '_options',
                // Page slug used to denote the panel
                'save_defaults'        => true,
                // On load save the defaults to DB before user clicks save or not
                'default_show'         => false,
                // If true, shows the default value next to each field that is not the default value.
                'default_mark'         => '',
                // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export'   => true,
                // Shows the Import/Export panel when not used as a field.

                // CAREFUL -> These options are for advanced use only
                'transient_time'       => 60 * MINUTE_IN_SECONDS,
                'output'               => true,
                // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'           => true,
                // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'             => '',
                // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'          => false,
                // REMOVE

                // HINTS
                'hints'                => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'   => 'light',
                        'shadow'  => true,
                        'rounded' => false,
                        'style'   => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show' => array(
                            'effect'   => 'slide',
                            'duration' => '500',
                            'event'    => 'mouseover',
                        ),
                        'hide' => array(
                            'effect'   => 'slide',
                            'duration' => '500',
                            'event'    => 'click mouseleave',
                        ),
                    ),
                )
            );

            $this->args['footer_credit'] = $theme->get( 'Name' ) . ' ' . $theme->get( 'Version' ) . esc_html__(' by Clapat', 'grenada');

        }

        public function validate_callback_function( $field, $value, $existing_value ) {
            $error = true;
            $value = 'just testing';

            $return['value'] = $value;
            $field['msg']    = 'your custom error message';
            if ( $error == true ) {
                $return['error'] = $field;
            }

            return $return;
        }

        public function class_field_callback( $field, $value ) {
            print_r( $field );
            echo '<br/>CLASS CALLBACK';
            print_r( $value );
        }

    }

    $GrenadaConfig = new grenada_options_config();
} else {
    echo esc_html__("The class named Redux_Framework_sample_config has already been called.", "grenada");
}

/**
 * Custom function for the callback referenced above
 */
if ( ! function_exists( 'redux_my_custom_field' ) ):
    function redux_my_custom_field( $field, $value ) {
        print_r( $field );
        echo '<br/>';
        print_r( $value );
    }
endif;

/**
 * Custom function for the callback validation referenced above
 * */
if ( ! function_exists( 'redux_validate_callback_function' ) ):
    function redux_validate_callback_function( $field, $value, $existing_value ) {
        $error = true;
        $value = 'just testing';

        $return['value'] = $value;
        $field['msg']    = 'your custom error message';
        if ( $error == true ) {
            $return['error'] = $field;
        }

        return $return;
    }
endif;
