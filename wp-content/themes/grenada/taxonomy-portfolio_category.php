<?php
// archive template for portfolio categories

get_header();

$grenada_hover_effect			= "below-caption";
$grenada_portfolio_layout	= "wide";

$grenada_page_container_list = array();
?>
				
		<!-- Main -->
		<div id="main">
		
			<!-- Hero Section -->
			<div id="hero">
			   <div id="hero-styles" class="parallax-onscroll">
					<div id="hero-caption" class="">
						<div class="inner">
							<h1 class="hero-title"><?php single_cat_title(); ?></h1> 
						</div>
					</div>                    
				</div>
			</div>                      
			<!--/Hero Section -->

			<!-- Main Content -->
			<div id="main-content">
				<div id="main-page-content">
				
					<!-- Portfolio -->
					<div id="portfolio-wrap" class="<?php echo sanitize_html_class( $grenada_portfolio_layout ); ?>">
						<div id="portfolio"  class="<?php echo sanitize_html_class( $grenada_hover_effect ); ?> <?php if( grenada_get_theme_options('clapat_grenada_enable_ajax') ){ echo 'thumb-with-ajax'; } else { echo 'thumb-no-ajax'; } ?>">
						<?php

							while( have_posts() ){

								the_post();
								
								get_template_part('sections/portfolio_section_item');
								
								$grenada_item_full_image = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-hero-img' );

								if( $grenada_item_full_image && isset( $grenada_item_full_image['url'] ) ){
								
									$grenada_page_container_list[] = $grenada_item_full_image['url'];
								}
								
							}
							
							wp_reset_postdata();
							
							grenada_portfolio_images( $grenada_page_container_list );
						?>
						</div>
					</div>
					<!--/Portfolio -->
					
				</div>
                <!--/Main Page Content -->
			</div>
			<!-- /Main Content -->
		</div>
        <!--/Main -->
<?php
	
get_footer();

?>