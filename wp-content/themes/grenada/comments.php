<?php
		
	if ( post_password_required() ) { ?>
		<div class="bottom-post"><h4><?php esc_html_e('This post is password protected. Enter the password to view comments.', 'grenada'); ?></h4></div>
	<?php
		return;
	}
        
?>



	
<!-- Article Discusion -->
<div id="comments" class="post-comments">
	
		<div class="article-head">                            
			<ul class="entry-meta">
				<li><?php comments_number(esc_html__('No Comments', 'grenada'), esc_html__('One Comment', 'grenada'), esc_html__('% Comments', 'grenada'));?></li>
			</ul>
		</div>
			
		<?php if ( have_comments() ) { ?>

			<div class="comments-navigation">
				<div class="alignleft"><?php previous_comments_link(); ?></div>
				<div class="alignright"><?php next_comments_link(); ?></div>
			</div>

			<div class="container">
				<?php wp_list_comments('callback=grenada_comment&style=div'); ?>
			</div>
					
			<div class="comments-navigation">
				<div class="alignleft"><?php previous_comments_link(); ?></div>
				<div class="alignright"><?php next_comments_link(); ?></div>
			</div>

		<?php } // if have comments ?>

		<?php if ( !comments_open() ) { ?>
		<!-- If comments are closed. -->
		<h5><?php esc_html_e('Comments are closed.', 'grenada'); ?></h5>
		<?php } ?>

</div>
<!-- Article Discusion -->




<?php if ( comments_open() ) { ?>

	<!-- Post Comments Formular -->
	<div class="post-form">

<?php  

        $grenada_class_message_area = '';
        if( is_user_logged_in() ){

            $grenada_class_message_area = 'comment_area_loggedin';
        }

        $grenada_comment_id = "comment";
                            
        $grenada_commentform_args = array(
                        'id_form'           	=> 'commentsform',
						'class_form'      	=> 'comment-form',
						'class_submit'		=> 'submit hide-ball',
                        'id_submit'         	=> 'submit',
                        'title_reply'       		=> '<div class="article-head"><ul class="entry-meta"><li>'. wp_kses_post( esc_html__( 'Leave a comment', 'grenada') ) . '</li></ul></div>',
                        'title_reply_to'    	=> '<div class="article-head"><ul class="entry-meta"><li>'. wp_kses_post( esc_html__( 'Leave a comment to %s ', 'grenada') ) . '</li></ul></div>',
                        'cancel_reply_link' => '<span class="cancel-reply">'. wp_kses_post( esc_html__( 'Cancel Reply', 'grenada') ) . '</span>',
                        'label_submit'      	=> wp_kses_post( esc_html__( 'Post Comment', 'grenada') ),

                        'comment_field' =>  '<div class="message-box hide-ball ' . $grenada_class_message_area . '"><textarea id="' . $grenada_comment_id . '" name="comment" onfocus="if(this.value == \'' . esc_attr__( 'Comment', 'grenada') .'\') { this.value = \'\'; }" onblur="if(this.value == \'\') { this.value = \'' . esc_attr__( 'Comment', 'grenada') .'\'; }" >' . esc_html__( 'Comment', 'grenada') .'</textarea><label class="input_label slow"></label></div>',

                        'must_log_in' => '<p class="must-log-in">' .
                                         sprintf(
                                        wp_kses_post( __( '<h4>You must be <a href="%s">logged in</a> to post a comment.</h4>', 'grenada') ),
                                        wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
                                        ) . '</p>',
            
                        'comment_notes_before' => '',

                        'comment_notes_after' => '',
            
                        'fields' => apply_filters( 'comment_form_default_fields', array(

                                                    'author' => '<div class="name-box hide-ball"><input name="author" type="text" id="author" size="30"  onfocus="if(this.value == \'' . esc_attr__( 'Name', 'grenada') .'\') { this.value = \'\'; }" onblur="if(this.value == \'\') { this.value = \'' . esc_attr__( 'Name', 'grenada') .'\'; }" value=\'' . esc_attr__( 'Name', 'grenada') . '\' ><label class="input_label"></label></div>',

                                                    'email' =>  '<div class="email-box hide-ball"><input name="email" type="text" id="email" size="30"  onfocus="if(this.value == \'' . esc_attr__( 'E-mail', 'grenada') .'\') { this.value = \'\'; }" onblur="if(this.value == \'\') { this.value = \'' . esc_attr__( 'E-mail', 'grenada') .'\'; }" value=\'' . esc_attr__( 'E-mail', 'grenada') . '\' ><label class="input_label"></label></div>',

                                                    'url' => ''
                                                    )
                                                )
                    );
                
        comment_form( $grenada_commentform_args );

?>
        </div>
		<!-- /Post Comments Formular -->
<?php

} // if comments are open
?>