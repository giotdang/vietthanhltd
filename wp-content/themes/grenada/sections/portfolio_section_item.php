<?php

$full_image = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-hero-img' );

if( $full_image && isset( $full_image['url'] ) ){

    $grenada_item_classes 		= '';
    $grenada_item_categories 	= '';
	$grenada_item_cats = get_the_terms($post->ID, 'portfolio_category');
	if($grenada_item_cats){

		foreach($grenada_item_cats as $item_cat) {
            $grenada_item_classes 		.= $item_cat->slug . ' ';
            $grenada_item_categories 	.= $item_cat->name . ', ';
        }

		$grenada_item_categories = rtrim($grenada_item_categories, ', ');

	}

    $item_height = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-thumbnail-size' );
	$grenada_item_classes .= $item_height;

	$grenada_case_caption = grenada_get_theme_options( 'clapat_grenada_portfolio_case_caption' );
	
	$item_url = get_the_permalink();

?>
						<div class="item <?php echo esc_attr( $grenada_item_classes ); ?>">
                            <div class="item-appear">                                    		
                                <div class="item-content">                            	                                    
									<a class="item-wrap ajax-link-project" data-type="page-transition" href="<?php echo esc_url( $item_url ); ?>"><?php if( grenada_get_theme_options('clapat_grenada_enable_ajax') ){ ?></a><?php } ?>
									<div class="item-wrap-image">
										<div class="item-image" data-src="<?php echo esc_url( $full_image['url'] ); ?>">
											<?php if( grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-video' ) ){ 
												$grenada_video_webm_url = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-video-webm' );
												$grenada_video_mp4_url = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-video-mp4' );
											?>
											<div class="hero-video-wrapper">
												<video loop muted class="bgvid">
													<?php if( !empty( $grenada_video_mp4_url ) ){ ?>}
													<source src="<?php echo esc_url( $grenada_video_mp4_url ); ?>" type="video/mp4">
													<?php } ?>
													<?php if( !empty( $grenada_video_webm_url ) ){ ?>}
													<source src="<?php echo esc_url( $grenada_video_webm_url ); ?>" type="video/webm">
													<?php } ?>
												</video>
											</div>
											<?php } ?>
										</div>
									</div>
									<?php if( !grenada_get_theme_options('clapat_grenada_enable_ajax') ){ ?></a><?php } ?>
									<div class="item-caption">                                
										<h2 class="item-title"><?php the_title(); ?></h2>
										<h4 class="item-cat"><span data-hover="<?php echo wp_kses_post( $grenada_case_caption ); ?>"><?php echo wp_kses_post( $grenada_item_categories ); ?></span></h4>
                                    </div>
								</div>
                            </div>                                        
                        </div>

<?php

}
?>
