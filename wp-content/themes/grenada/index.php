<?php

get_header();

if ( have_posts() ){

?>

	<?php 
		
		// display sidebar section, if defined
		get_template_part('sections/blog_sidebar_section'); 
		
	?>
	
	<!-- Main -->
	<div id="main">
	
		<!-- Hero Section -->
        <div id="hero">
           <div id="hero-styles" class="parallax-onscroll">
                <div id="hero-caption" class="">
                    <div class="inner">
                        <h1 class="hero-title"><?php  echo wp_kses_post( grenada_get_theme_options('clapat_grenada_blog_default_title') ); ?></h1> 
                    </div>
                </div>                    
            </div>
        </div>                      
        <!--/Hero Section -->
		
    	<!-- Main Content -->
    	<div id="main-content">
			<!-- Blog-->
			<div id="blog">
				<!-- Blog-Content-->
				<div id="blog-content">
				<?php 
						
					// the loop
					while( have_posts() ){

						the_post();

						get_template_part( 'sections/blog_post_section' );
						
					}
							
				?>
				<!-- /Blog-Content-->
				</div>
				<?php
						
					grenada_pagination();

				?>
			</div>
			<!-- /Blog-->
		</div>
		<!--/Main Content-->
	</div>
	<!-- /Main -->
<?php

}
	
get_footer();

?>