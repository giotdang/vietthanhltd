			<?php
				
				$grenada_menu_breakpoint = "1025";
				if( grenada_get_theme_options( 'clapat_grenada_enable_fullscreen_menu' ) ){
					
					$grenada_menu_breakpoint = "10025";
				}
				
				wp_nav_menu(array(
					'theme_location' 	=> 'primary-menu',
					'container' 			=> 'nav',
					'items_wrap' 		=> '<div class="nav-height"><div class="outer"><div class="inner"><ul id="%1$s" data-breakpoint="' . $grenada_menu_breakpoint . '" class="flexnav %2$s">%3$s</ul></div></div></div>'
				));

			?>
