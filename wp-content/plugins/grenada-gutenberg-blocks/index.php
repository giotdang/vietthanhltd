<?php

/*
Plugin Name: Grenada Gutenberg Blocks
Plugin URI: http://themeforest.net/user/ClaPat/portfolio
Description: Gutenberg editor blocks for Grenada WordPress Themes
Version: 1.1
Author: Clapat
Author URI: http://themeforest.net/user/ClaPat/
*/

defined( 'ABSPATH' ) || exit;

include 'blocks/index.php';
