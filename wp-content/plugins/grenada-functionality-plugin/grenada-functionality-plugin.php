<?php
/*
Plugin Name: Grenada Functionality Plugin
Plugin URI: http://themeforest.net/user/ClaPat/portfolio
Description: Shortcodes and Custom Post Types for Grenada WordPress Themes
Version: 2.1
Author: Clapat
Author URI: http://themeforest.net/user/ClaPat/
*/

if( !defined('GRENADA_SHORTCODES_DIR_URL') ) define('GRENADA_SHORTCODES_DIR_URL', plugin_dir_url(__FILE__));
if( !defined('GRENADA_SHORTCODES_DIR') ) define('GRENADA_SHORTCODES_DIR', plugin_dir_path(__FILE__));

// metaboxes
require_once( GRENADA_SHORTCODES_DIR . '/metaboxes/init.php' );

// load plugin's text domain
add_action( 'plugins_loaded', 'grenada_shortcodes_load_textdomain' );
function grenada_shortcodes_load_textdomain() {
    load_plugin_textdomain( 'clapat_grenada_plugin_text_domain', false, dirname( plugin_basename( __FILE__ ) ) . '/include/langs' );
}

// custom post types
require_once( GRENADA_SHORTCODES_DIR . '/include/custom-post-types-config.php' );

// grenada shortcodes
require_once( GRENADA_SHORTCODES_DIR . '/include/shortcodes.php' );

?>