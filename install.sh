yum update -y && yum install -y epel-release
yum install epel-release yum-utils -y
yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum-config-manager --enable remi-php72
yum install -y php-cli php-fpm php-mysql php-json php-opcache php-mbstring php-xml php-gd php-curl
yum install -y mariadb-client mariadb-server
yum install -y nginx php-fpm
