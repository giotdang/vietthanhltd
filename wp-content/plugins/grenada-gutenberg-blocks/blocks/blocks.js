/**
 * Grenada Shortcodes Gutenberg Blocks
 *
 */
( function( blocks, blockEditor, i18n, element, components ) {
	var el = element.createElement;
	var __ = i18n.__;
	var RichText = blockEditor.RichText;
	var PlainText = blockEditor.PlainText;
	var MediaPlaceHolder = blockEditor.MediaPlaceHolder;
	var TextControl = components.TextControl;
	var TextareaControl = components.TextareaControl;
	var RangeControl = components.RangeControl;
	var ColorPaletteControl = components.ColorPalette;
	var SelectControl = components.SelectControl;
	var InspectorControls = blockEditor.InspectorControls;
	var MediaUpload = blockEditor.MediaUpload;
	var InnerBlocks = blockEditor.InnerBlocks;
	var AlignmentToolbar = blockEditor.AlignmentToolbar;
	var BlockControls = blockEditor.BlockControls;
 	
	/** Utils **/
	function normalizeUndef( x ){
		
		if (typeof x === 'undefined'){
			
			 return '';
		}
		else{
			
			return x;
		}
	}
	
	/** Container **/
	blocks.registerBlockType( 'grenada-gutenberg/container', {
		title: __( 'Grenada: Container', 'grenada-gutenberg' ),
		icon: 'analytics',
		category: 'layout',
		attributes: {
			background_color: {
				type: 'string',
				default: '#f2f2f2'
			},
			padding_top: {
				type: 'number',
				default: 0
			},
			padding_bottom: {
				type: 'number',
				default: 0
			},
			padding_left: {
				type: 'number',
				default: 0
			},
			padding_right: {
				type: 'number',
				default: 0
			},
			alignment: {
				type: 'string',
				default: 'left'
			},
		}, 
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'container', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			const colors = [ 
				{ name: 'Default', color: '#f2f2f2' }, 
				{ name: 'White', color: '#ffffff' }, 
				{ name: 'Light Grey', color: '#bbbbbb' }, 
				{ name: 'Dark Grey', color: '#555555' }, 
				{ name: 'Black', color: '#000' },
			];
			
			function onChangeAlignment( newAlignment ) {
				props.setAttributes( { alignment: newAlignment === undefined ? 'none' : newAlignment } );
			}
			
			return	[ el( BlockControls,
							{ key: 'controls' },
							el(
								AlignmentToolbar,
								{
									value: props.attributes.alignment,
									onChange: onChangeAlignment,
								}
							)
						),
						el( 'div', { className: 'clapat-editor-block-wrapper clapat-editor-container'},
							el( 'h4', { className: 'clapat-editor-block-title' }, __('Grenada Container', 'grenada-gutenberg' )),
							el( InnerBlocks, {} ),
							/*
							 * InspectorControls lets you add controls to the Block sidebar.
							 */
							el( InspectorControls, {},
								el( 'div', { className: 'components-panel__body is-opened' }, 
									el( 'strong', {}, __('Select Background Color:',  'grenada-gutenberg') ),
									el( 'div', { className : 'clapat-color-palette' },
										el( ColorPaletteControl, {
											colors: colors,
											value: props.attributes.background_color,
											onChange: ( value ) => { 
												props.setAttributes( { background_color: value === undefined ? 'transparent' : value } ); 
											},
										} )
									),
									el( 'div', { className : 'clapat-range-control' }, 
										el( RangeControl, {
											label: __('Padding Top (in pixels)',  'grenada-gutenberg'),
											value: props.attributes.padding_top,
											onChange: ( value ) => { 
												if (typeof value === "undefined") return;
												props.setAttributes( { padding_top: value } ); 
											},
											type: 'number',
											step: 10,
											min: 0,
											max: 100
										} ) ),
									el( 'div', { className : 'clapat-range-control' }, 
										el( RangeControl, {
											label: __('Padding Bottom (in pixels)',  'grenada-gutenberg'),
											value: props.attributes.padding_bottom,
											onChange: ( value ) => {
												if (typeof value === "undefined") return;
												props.setAttributes( { padding_bottom: value } ); 
											},
											type: 'number',
											step: 10,
											min: 0,
											max: 100
										} ) ),
									el( 'div', { className : 'clapat-range-control' }, 
										el( RangeControl, {
											label: __('Padding Left (in pixels)',  'grenada-gutenberg'),
											value: props.attributes.padding_left,
											onChange: ( value ) => {
												if (typeof value === "undefined") return;
												props.setAttributes( { padding_left: value } );
											},
											type: 'number',
											step: 10,
											min: 0,
											max: 100
										} ) ),
									el( 'div', { className : 'clapat-range-control' }, 
										el( RangeControl, {
											label: __('Padding Right (in pixels)',  'grenada-gutenberg'),
											value: props.attributes.padding_right,
											onChange: ( value ) => {
												if (typeof value === "undefined") return;
												props.setAttributes( { padding_right: value } );
											},
											type: 'number',
											step: 10,
											min: 0,
											max: 100
										} ) )
									)
								)
							) ];
		},

		save: function( props ) {
			
			return el( 'div', 
							{ 
								className: props.className,
								style : {
									'background-color': props.attributes.background_color,
									'padding-top': props.attributes.padding_top + 'px',
									'padding-bottom': props.attributes.padding_bottom + 'px',
									'padding-left': props.attributes.padding_left + 'px',
									'padding-right': props.attributes.padding_right + 'px',
									'text-align': props.attributes.alignment
								}
							}, InnerBlocks.Content() );
	
		},
	} );
	
	/** Button **/
	blocks.registerBlockType( 'grenada-gutenberg/button', {
		title: __( 'Grenada: Button', 'grenada-gutenberg' ),
		icon: 'editor-removeformatting',
		category: 'layout',
		attributes: {
			caption: {
				type: 'string',
				default: __( 'Caption', 'grenada-gutenberg' )
			},
			link: {
				type: 'string',
				default: 'http://'
			},
			target: {
				type: 'string',
				default: '_blank'
			},
			type: {
				type: 'string',
				default: 'normal'
			},
		},
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'button', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			return [
				
				 el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
						el( 'h4', { className: 'clapat-editor-block-title' }, __('Grenada Button', 'grenada-gutenberg' )),
						
						el( PlainText,
						{
							className: 'clapat-inline-caption',
							value: props.attributes.caption,
							onChange: ( value ) => { props.setAttributes( { caption: value } ); },
						}),
						el( PlainText,
						{
							className: 'clapat-inline-content',
							value: props.attributes.link,
							onChange: ( value ) => { props.setAttributes( { link: value } ); },
						}),
						
						/*
						 * InspectorControls lets you add controls to the Block sidebar.
						 */
						el( InspectorControls, {},
							el( 'div', { className: 'components-panel__body is-opened' }, 
								el( SelectControl, {
									label: __('Type', 'grenada-gutenberg'),
									value: props.attributes.type,
									options : [
										{ label: __('Normal', 'grenada-gutenberg'), value: 'normal' },
										{ label: __('Outline',  'grenada-gutenberg'), value: 'outline' },
									],
									onChange: ( value ) => { props.setAttributes( { type: value } ); },
								} ),
								el( SelectControl, {
									label: __('Link Target', 'grenada-gutenberg'),
									value: props.attributes.target,
									options: [
										{ label: 'Blank', value: '_blank' },
										{ label: 'Self', value: '_self' },
									],
									onChange: ( value ) => { props.setAttributes( { target: value } ); },
								} ),
							),
						),
					)
			]
		},
		save: function( props ) {
		
			return '[button link="' + props.attributes.link + '" target="' + props.attributes.target + '" type="' + props.attributes.type + '" extra_class_name=""]' + props.attributes.caption + '[/button]'; 
		},
	} );
	
	/** Title **/
	blocks.registerBlockType( 'grenada-gutenberg/title', {
		title: __( 'Grenada: Title', 'grenada-gutenberg' ),
		icon: 'editor-textcolor',
		category: 'layout',
		attributes: {
			caption: {
				type: 'string',
				default: 'Title'
			},
			size: {
				type: 'string',
				default: 'h1'
			},
			underline: {
				type: 'string',
				default: 'no'
			},
			big: {
				type: 'string',
				default: 'no'
			}
		},
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'title', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			if( props.attributes.underline == 'yes'){
				
				props.className = 'title-has-line';
			}
			if( props.attributes.big == 'yes'){
				
				props.className += ' big-title';
			}
			
			return [
				
				 el(  props.attributes.size, { className: props.className }, props.attributes.caption ),
				 
				/*
				 * InspectorControls lets you add controls to the Block sidebar.
				 */
				el( InspectorControls, {},
					el( 'div', { className: 'components-panel__body is-opened' }, 
						el( TextControl, {
							label: __('Caption', 'grenada-gutenberg'),
							value: props.attributes.caption,
							onChange: ( value ) => { props.setAttributes( { caption: value } ); },
						} ),
						el( SelectControl, {
							label: __('Size', 'grenada-gutenberg'),
							value: props.attributes.size,
							options: [
								{ label: 'H1', value: 'h1' },
								{ label: 'H2', value: 'h2' },
								{ label: 'H3', value: 'h3' },
								{ label: 'H4', value: 'h4' },
								{ label: 'H5', value: 'h5' },
								{ label: 'H6', value: 'h6' },
							],
							onChange: ( value ) => { props.setAttributes( { size: value } ); },
						} ),
						el( SelectControl, {
							label: __('Underline',  'grenada-gutenberg'),
							value: props.attributes.underline,
							options : [
								{ label: __('Yes',  'grenada-gutenberg'), value: 'yes' },
								{ label: __('No',  'grenada-gutenberg'), value: 'no' },
							],
							onChange: ( value ) => { props.setAttributes( { underline: value } ); },
						} ),
						el( SelectControl, {
							label: __('Big', 'grenada-gutenberg'),
							value: props.attributes.big,
							options: [
								{ label: __('Yes',  'grenada-gutenberg'), value: 'yes' },
								{ label: __('No',  'grenada-gutenberg'), value: 'no' },
							],
							onChange: ( value ) => { props.setAttributes( { big: value } ); },
						} ),
					),
				),
			]
		},
		save: function( props ) {
			
			if( props.attributes.underline == 'yes'){
				
				props.className = 'title-has-line';
			}
			if( props.attributes.big == 'yes'){
				
				props.className += ' big-title';
			}
			
			return el(  props.attributes.size, { className: props.className }, props.attributes.caption );
		},
	} );

	/** Icon Service **/
	blocks.registerBlockType( 'grenada-gutenberg/icon-service', {
		title: __( 'Grenada: Icon Service', 'grenada-gutenberg' ),
		icon: 'editor-justify',
		category: 'layout',
		attributes: {
			icon: {
				type: 'string',
				default: __( 'fa fa-cogs', 'grenada-gutenberg')
			},
			title: {
				type: 'string',
				default: __( 'Icon Service Title. Click to edit it.', 'grenada-gutenberg')
			},
			content: {
				type: 'string',
				default: __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non est nec orci ultricies fringilla. Nam ultrices sem in odio scelerisque, sed mollis magna tincidunt.', 'grenada-gutenberg')
			},
			
		},
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ),  __( 'icon', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			return [
				
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'h4', { className: 'clapat-editor-block-title' }, __( 'Grenada Icon Box', 'grenada-gutenberg' ) ),
					
					el( PlainText,
					{
						className: 'clapat-inline-caption',
						value: props.attributes.icon,
						onChange: ( value ) => { props.setAttributes( { icon: value } ); },
					}),
					
					el( PlainText,
					{
						className: 'clapat-inline-caption',
						value: props.attributes.title,
						onChange: ( value ) => { props.setAttributes( { title: value } ); },
					}),
					
					el( PlainText, {
						className: 'clapat-inline-content',
						value: props.attributes.content,
						onChange: ( value ) => { props.setAttributes( { content: value } ); },
					} ),
					
				),
				 
			]
		},
		save: function( props ) {
			
			return '[service icon="' + props.attributes.icon + '" title="' + props.attributes.title + '" extra_class_name=""]' + props.attributes.content + '[/service]'; 
		},
	} );
	
	/** Contact Box **/
	blocks.registerBlockType( 'grenada-gutenberg/contact-box', {
		title: __( 'Grenada: Contact Box', 'grenada-gutenberg' ),
		icon: 'phone',
		category: 'layout',
		attributes: {
			icon: {
				type: 'string',
				default: __( 'fa fa-envelope', 'grenada-gutenberg')
			},
			title: {
				type: 'string',
				default: __( 'Contact Box Title. Click to edit it.', 'grenada-gutenberg')
			},
			content: {
				type: 'string',
				default: __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non est nec orci ultricies fringilla. Nam ultrices sem in odio scelerisque, sed mollis magna tincidunt.', 'grenada-gutenberg')
			},
			
		},
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ),  __( 'contact', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			return [
				
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'h4', { className: 'clapat-editor-block-title' }, __( 'Grenada Icon Box', 'grenada-gutenberg' ) ),
					
					el( PlainText,
					{
						className: 'clapat-inline-caption',
						value: props.attributes.icon,
						onChange: ( value ) => { props.setAttributes( { icon: value } ); },
					}),
					
					el( PlainText,
					{
						className: 'clapat-inline-caption',
						value: props.attributes.title,
						onChange: ( value ) => { props.setAttributes( { title: value } ); },
					}),
					
					el( PlainText, {
						className: 'clapat-inline-content',
						value: props.attributes.content,
						onChange: ( value ) => { props.setAttributes( { content: value } ); },
					} ),
					
				),
				 
			]
		},
		save: function( props ) {
			
			return '[contact_box icon="' + props.attributes.icon + '" title="' + props.attributes.title + '" extra_class_name=""]' + props.attributes.content + '[/contact_box]'; 
		},
	} );
	
	/** Image Collage **/
	const template_clapat_collage = [
	  [ 'grenada-gutenberg/collage-image', {} ], // [ blockName, attributes ]
	];
	
	blocks.registerBlockType( 'grenada-gutenberg/collage', {
		title: __( 'Grenada: Collage', 'grenada-gutenberg' ),
		icon: 'layout',
		category: 'layout',
		allowedBlocks: ['grenada-gutenberg/collage-image'],
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'collage', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			return	el( 'div', { className: 'clapat-editor-block-wrapper clapat-editor-collage'},
							el( 'h4', { className: 'clapat-editor-block-title' }, __('Grenada Collage', 'grenada-gutenberg' )),
							el( InnerBlocks, {allowedBlocks: ['grenada-gutenberg/collage-image'], template: template_clapat_collage} )
						);

		},

		save: function( props ) {
			
			return el( 'div', {id: 'justified-grid', className: props.className }, InnerBlocks.Content() );
	
		},
	} );
	
	blocks.registerBlockType( 'grenada-gutenberg/collage-image', {
		title: __( 'Grenada: Collage Image', 'grenada-gutenberg' ),
		icon: 'format-image',
		category: 'layout',
		parent: [ 'grenada-gutenberg/collage' ],

		attributes: {
			thumb_image: {
				type: 'string',
				default: ''
			},
			thumb_image_id: {
				type: 'number',
			},
			full_image: {
				type: 'string',
				default: ''
			},
			full_image_id: {
				type: 'number',
			},
			alt: {
				type: 'string',
				default: ''
			},
			info: {
				type: 'string',
				default: __( 'Caption Text', 'grenada-gutenberg' )
			},
		},

		edit: function( props ) {
			
			var onSelectThumbnailImage = function( media ) {
				return props.setAttributes( {
					thumb_image: media.url,
					thumb_image_id: media.id,
				} );
			};
			var onSelectImage = function( media ) {
				return props.setAttributes( {
					full_image: media.url,
					full_image_id: media.id,
				} );
			};
			return [
			
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'div', { className: 'clapat-editor-image' },
						el( MediaUpload, {
							onSelect: onSelectThumbnailImage,
							type: 'image',
							value: props.attributes.thumb_image_id,
							render: function( obj ) {
								return el( components.Button, {
										className: props.attributes.thumb_image_id ? 'clapat-image-added' : 'button button-large',
										onClick: obj.open
									},
									! props.attributes.thumb_image_id ? i18n.__( 'Upload Collage Thumbnail Image', 'grenada-gutenberg' ) : el( 'img', { src: props.attributes.thumb_image } ),
									el ('p', {}, __( 'Thumb Image', 'grenada-gutenberg' ) )
								);
							}
						} )
					),
					
					el( 'div', { className: 'clapat-editor-image' },
						el( MediaUpload, {
							onSelect: onSelectImage,
							type: 'image',
							value: props.attributes.full_image_id,
							render: function( obj ) {
								return el( components.Button, {
										className: props.attributes.full_image_id ? 'clapat-image-added' : 'button button-large',
										onClick: obj.open
									},
									! props.attributes.full_image_id ? i18n.__( 'Upload Collage Full Image', 'grenada-gutenberg' ) : el( 'img', { src: props.attributes.full_image } ),
									el ('p', {}, __( 'Full Image', 'grenada-gutenberg' ) )
								);
							}
						} )
					),

				),
				/*
				 * InspectorControls lets you add controls to the Block sidebar.
				 */
				el( InspectorControls, {},
					el( 'div', { className: 'components-panel__body is-opened' }, 
						el( TextControl, {
							label: __( 'ALT attribute', 'grenada-gutenberg' ),
							value: props.attributes.alt,
							onChange: ( value ) => { props.setAttributes( { alt: value } ); },
						} ),
						
						el( TextControl, {
							label: __( 'Collage Image Info', 'grenada-gutenberg' ),
							value: props.attributes.info,
							onChange: ( value ) => { props.setAttributes( { info: value } ); },
						} ),
					),
				),
			];
		},

		save: function( props ) {
			
			return '[clapat_collage_image thumb_img_url="' + props.attributes.thumb_image + '" img_url="' + props.attributes.full_image + '" alt="' + props.attributes.alt + '" info="' + props.attributes.info + '"][/clapat_collage_image]'; 

		},
	} );
	
	/** Image Carousel **/
	const template_clapat_image_carousel = [
	  [ 'grenada-gutenberg/carousel-image', {} ], // [ blockName, attributes ]
	];
	
	blocks.registerBlockType( 'grenada-gutenberg/carousel', {
		title: __( 'Grenada: Image Carousel', 'grenada-gutenberg' ),
		icon: 'slides',
		category: 'layout',
		allowedBlocks: ['grenada-gutenberg/carousel-image'],
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'carousel', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			return	el( 'div', { className: 'clapat-editor-block-wrapper clapat-editor-carousel'},
							el( 'h4', { className: 'clapat-editor-block-title' }, __('Grenada Carousel', 'grenada-gutenberg' )),
							el( InnerBlocks, {allowedBlocks: ['grenada-gutenberg/carousel-image'], template: template_clapat_image_carousel} )
						);

		},

		save: function( props ) {
			
			return el( 'div', { className: 'carousel' }, InnerBlocks.Content() );
	
		},
	} );
	
	blocks.registerBlockType( 'grenada-gutenberg/carousel-image', {
		title: __( 'Grenada: Carousel Image', 'grenada-gutenberg' ),
		icon: 'format-image',
		category: 'layout',
		parent: [ 'grenada-gutenberg/carousel' ],

		attributes: {
			img_url: {
				type: 'string',
				default: ''
			},
			img_id: {
				type: 'number',
			},
			alt: {
				type: 'string',
				default: ''
			},
		},

		edit: function( props ) {
			
			var onSelectImage = function( media ) {
				return props.setAttributes( {
					img_url: media.url,
					img_id: media.id,
				} );
			};
			return [
			
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'div', { className: 'clapat-editor-image' },
						el( MediaUpload, {
							onSelect: onSelectImage,
							type: 'image',
							value: props.attributes.img_id,
							render: function( obj ) {
								return el( components.Button, {
										className: props.attributes.img_id ? 'clapat-image-added' : 'button button-large',
										onClick: obj.open
									},
									! props.attributes.img_id ? i18n.__( 'Upload Carousel Image', 'grenada-gutenberg' ) : el( 'img', { src: props.attributes.img_url } ),
									el ('p', {}, __( 'Carousel Image', 'grenada-gutenberg' ) )
								);
							}
						} )
					),

				),
				/*
				 * InspectorControls lets you add controls to the Block sidebar.
				 */
				el( InspectorControls, {},
					el( 'div', { className: 'components-panel__body is-opened' }, 
						el( TextControl, {
							label: __( 'ALT attribute', 'grenada-gutenberg' ),
							value: props.attributes.alt,
							onChange: ( value ) => { props.setAttributes( { alt: value } ); },
						} ),
					),
				),
			];
		},

		save: function( props ) {
			
			return '[carousel_slide img_url="' + props.attributes.img_url + '" alt="' + props.attributes.alt + '"][/carousel_slide]'; 

		},
	} );
	
	/** Image Slider **/
	const template_clapat_image_slider = [
	  [ 'grenada-gutenberg/slider-image', {} ], // [ blockName, attributes ]
	];
	
	blocks.registerBlockType( 'grenada-gutenberg/slider', {
		title: __( 'Grenada: Image Slider', 'grenada-gutenberg' ),
		icon: 'images-alt2',
		category: 'layout',
		allowedBlocks: ['grenada-gutenberg/slider-image'],
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'slider', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			return	el( 'div', { className: 'clapat-editor-block-wrapper clapat-editor-slider'},
							el( 'h4', { className: 'clapat-editor-block-title' }, __('Grenada Slider', 'grenada-gutenberg' )),
							el( InnerBlocks, {allowedBlocks: ['grenada-gutenberg/slider-image'], template: template_clapat_image_slider} )
						);

		},

		save: function( props ) {
			
			return el( 'div', { className: 'slider' }, InnerBlocks.Content() );
	
		},
	} );
	
	blocks.registerBlockType( 'grenada-gutenberg/slider-image', {
		title: __( 'Grenada: Slider Image', 'grenada-gutenberg' ),
		icon: 'format-image',
		category: 'layout',
		parent: [ 'grenada-gutenberg/slider' ],

		attributes: {
			img_url: {
				type: 'string',
				default: ''
			},
			img_id: {
				type: 'number',
			},
			alt: {
				type: 'string',
				default: ''
			},
		},

		edit: function( props ) {
			
			var onSelectImage = function( media ) {
				return props.setAttributes( {
					img_url: media.url,
					img_id: media.id,
				} );
			};
			return [
			
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'div', { className: 'clapat-editor-image' },
						el( MediaUpload, {
							onSelect: onSelectImage,
							type: 'image',
							value: props.attributes.img_id,
							render: function( obj ) {
								return el( components.Button, {
										className: props.attributes.img_id ? 'clapat-image-added' : 'button button-large',
										onClick: obj.open
									},
									! props.attributes.img_id ? i18n.__( 'Upload Slider Image', 'grenada-gutenberg' ) : el( 'img', { src: props.attributes.img_url } ),
									el ('p', {}, __( 'Slider Image', 'grenada-gutenberg' ) )
								);
							}
						} )
					),

				),
				/*
				 * InspectorControls lets you add controls to the Block sidebar.
				 */
				el( InspectorControls, {},
					el( 'div', { className: 'components-panel__body is-opened' }, 
						el( TextControl, {
							label: __( 'ALT attribute', 'grenada-gutenberg' ),
							value: props.attributes.alt,
							onChange: ( value ) => { props.setAttributes( { alt: value } ); },
						} ),
					),
				),
			];
		},

		save: function( props ) {
			
			return '[general_slide img_url="' + props.attributes.img_url + '" alt="' + props.attributes.alt + '"][/general_slide]'; 

		},
	} );
		
	/** Popup Image **/
	blocks.registerBlockType( 'grenada-gutenberg/popup-image', {
		title: __( 'Grenada: Popup Image', 'grenada-gutenberg' ),
		icon: 'format-image',
		category: 'layout',
		
		attributes: {
			thumb_image: {
				type: 'string',
				default: ''
			},
			thumb_image_id: {
				type: 'number',
			},
			full_image: {
				type: 'string',
				default: ''
			},
			full_image_id: {
				type: 'number',
			},
			
		},

		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'popup', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			var onSelectThumbnailImage = function( media ) {
				return props.setAttributes( {
					thumb_image: media.url,
					thumb_image_id: media.id,
				} );
			};
			var onSelectImage = function( media ) {
				return props.setAttributes( {
					full_image: media.url,
					full_image_id: media.id,
				} );
			};
				
			return [
			
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'h4', { className: 'clapat-editor-block-title' }, __( 'Clapat Popup Image', 'grenada-gutenberg' )),
					
					el( 'div', { className: 'clapat-editor-image' },
						el( MediaUpload, {
							onSelect: onSelectThumbnailImage,
							type: 'image',
							value: props.attributes.thumb_image_id,
							render: function( obj ) {
								return el( components.Button, {
										className: props.attributes.thumb_image_id ? 'clapat-image-added' : 'button button-large',
										onClick: obj.open
									},
									! props.attributes.thumb_image_id ? i18n.__( 'Upload Popup Thumbnail Image', 'grenada-gutenberg' ) : el( 'img', { src: props.attributes.thumb_image } ),
									el ('p', {}, __( 'Thumb Image', 'grenada-gutenberg' ) )
								);
							}
						} )
					),
					
					el( 'div', { className: 'clapat-editor-image' },
						el( MediaUpload, {
							onSelect: onSelectImage,
							type: 'image',
							value: props.attributes.full_image_id,
							render: function( obj ) {
								return el( components.Button, {
										className: props.attributes.full_image_id ? 'clapat-image-added' : 'button button-large',
										onClick: obj.open
									},
									! props.attributes.full_image_id ? i18n.__( 'Upload Popup Full Image', 'grenada-gutenberg' ) : el( 'img', { src: props.attributes.full_image } ),
									el ('p', {}, __( 'Full Image', 'grenada-gutenberg' ) )
								);
							}
						} )
					),

				),
				
			];
		},

		save: function( props ) {
			
			return '[clapat_popup_image img_url="' + props.attributes.full_image + '" thumb_url="' + props.attributes.thumb_image + '" extra_class_name=""][/clapat_popup_image]'; 

		},
	} );
	
	/** Captioned Image **/
	blocks.registerBlockType( 'grenada-gutenberg/captioned-image', {
		title: __( 'Grenada: Captioned Image', 'grenada-gutenberg' ),
		icon: 'format-image',
		category: 'layout',
		
		attributes: {
			full_image: {
				type: 'string',
				default: ''
			},
			full_image_id: {
				type: 'number',
			},
			caption: {
				type: 'string',
				default: __( 'Image Caption. Click to edit it', 'grenada-gutenberg')
			},
		},

		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'image', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			var onSelectImage = function( media ) {
				return props.setAttributes( {
					full_image: media.url,
					full_image_id: media.id,
				} );
			};
				
			return [
			
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'h4', { className: 'clapat-editor-block-title' }, __( 'Grenada Captioned Image', 'grenada-gutenberg' )),
					
					el( 'div', { className: 'clapat-editor-image' },
						el( MediaUpload, {
							onSelect: onSelectImage,
							type: 'image',
							value: props.attributes.full_image_id,
							render: function( obj ) {
								return el( components.Button, {
										className: props.attributes.full_image_id ? 'clapat-image-added' : 'button button-large',
										onClick: obj.open
									},
									! props.attributes.full_image_id ? i18n.__( 'Upload Popup Full Image', 'grenada-gutenberg' ) : el( 'img', { src: props.attributes.full_image } ),
									el ('p', {}, __( 'Full Image', 'grenada-gutenberg' ) )
								);
							}
						} )
					),
					
					el( PlainText,
					{
						className: 'clapat-inline-content',
						value: props.attributes.caption,
						onChange: ( value ) => { props.setAttributes( { caption: value } ); },
					}),
		
				),
				
			];
		},

		save: function( props ) {
			
			return '[clapat_captioned_image img_url="' + props.attributes.full_image + '" caption="' + props.attributes.caption + '" extra_class_name=""][/clapat_captioned_image]'; 

		},
	} );
	
	/** Testimonials **/
	const template_clapat_testimonials = [
	  [ 'grenada-gutenberg/testimonial', {} ], // [ blockName, attributes ]
	];

	blocks.registerBlockType( 'grenada-gutenberg/testimonials', {
		title: __( 'Grenada: Testimonials', 'grenada-gutenberg' ),
		icon: 'editor-quote',
		category: 'layout',
		allowedBlocks: ['grenada-gutenberg/testimonial'],
	
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'testimonial', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			return	el( 'div', { className: 'clapat-editor-block-wrapper'},
							el( 'h3', { className: 'clapat-editor-block-title' }, __( 'Grenada Testimonials', 'grenada-gutenberg' )),
							el( InnerBlocks, {allowedBlocks: ['grenada-gutenberg/testimonial'], template: template_clapat_testimonials } )
						);

		},

		save: function( props ) {
			
			return el( 'div', { className: 'text-carousel' }, InnerBlocks.Content() );
	
		},
	} );
	
	blocks.registerBlockType( 'grenada-gutenberg/testimonial', {
		title: __( 'Grenada: Testimonial', 'grenada-gutenberg' ),
		icon: 'editor-quote',
		category: 'layout',
		parent: [ 'grenada-gutenberg/testimonials' ],

		attributes: {
			name: {
				type: 'string',
				default: __( 'Reviewer Name. Click to edit it.', 'grenada-gutenberg' )
			},
			content: {
				type: 'string',
				default: __( 'This is a review placeholder. Click to edit it.', 'grenada-gutenberg' )
			},
		},

		edit: function( props ) {
			
			var content = props.attributes.content;
			function onChangeContent( newContent ) {
				props.setAttributes( { content: newContent } );
			}
			
			return [
			
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'h3', { className: 'clapat-editor-block-title' }, __( 'Grenada - Testimonial', 'grenada-gutenberg' )),
					
					el( PlainText,
					{
						value: props.attributes.content,
						onChange: ( value ) => { props.setAttributes( { content: value } ); },
					}),
					
					el( PlainText, {
						value: props.attributes.name,
						onChange: ( value ) => { props.setAttributes( { name: value } ); },
					} ),
					
				),
				
			];
		},

		save: function( props ) {
			
			return '[testimonial name="' + props.attributes.name + '"]' + props.attributes.content + '[/testimonial]'; 

		},
	} );
	
	/** Clients **/
	const template_clapat_clients = [
	  [ 'grenada-gutenberg/client', {} ], // [ blockName, attributes ]
	];

	blocks.registerBlockType( 'grenada-gutenberg/clients', {
		title: __( 'Grenada: Clients', 'grenada-gutenberg' ),
		icon: 'businessman',
		category: 'layout',
		allowedBlocks: ['grenada-gutenberg/client'],
	
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'client', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			return	el( 'div', { className: 'clapat-editor-block-wrapper'},
							el( 'h3', { className: 'clapat-editor-block-title' }, __( 'Grenada Clients', 'grenada-gutenberg' )),
							el( InnerBlocks, {allowedBlocks: ['grenada-gutenberg/client'], template: template_clapat_clients } )
						);

		},

		save: function( props ) {
			
			return el( 'div', { className: 'clients-table' }, InnerBlocks.Content() );
	
		},
	} );
	
	blocks.registerBlockType( 'grenada-gutenberg/client', {
		title: __( 'Grenada: Client', 'grenada-gutenberg' ),
		icon: 'editor-quote',
		category: 'layout',
		parent: [ 'grenada-gutenberg/clients' ],

		attributes: {
			img_url: {
				type: 'string',
				default: ''
			},
			img_id: {
				type: 'number',
			},
		},

		edit: function( props ) {
			
			var onSelectImage = function( media ) {
				return props.setAttributes( {
					img_url: media.url,
					img_id: media.id,
				} );
			};
			
			return [
			
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'div', { className: 'clapat-editor-image' },
						el( MediaUpload, {
							onSelect: onSelectImage,
							type: 'image',
							value: props.attributes.img_id,
							render: function( obj ) {
								return el( components.Button, {
										className: props.attributes.img_id ? 'clapat-image-added' : 'button button-large',
										onClick: obj.open
									},
									! props.attributes.img_id ? i18n.__( 'Upload Client Image', 'grenada-gutenberg' ) : el( 'img', { src: props.attributes.img_url } ),
									el ('p', {}, __( 'Client Image', 'grenada-gutenberg' ) )
								);
							}
						} )
					),
					
				),
				
			];
		},

		save: function( props ) {
			
			return '[client_item img_url="' + props.attributes.img_url + '"][/client_item]'; 

		},
	} );
	
	/** Hosted Video **/
	blocks.registerBlockType( 'grenada-gutenberg/video-hosted', {
		title: __( 'Grenada: Hosted Video', 'grenada-gutenberg' ),
		icon: 'video-alt',
		category: 'layout',
		attributes: {
			cover_image: {
				type: 'string',
				default: ''
			},
			cover_image_id: {
				type: 'number',
			},
			webm_url: {
				type: 'string',
				default: 'http://'
			},
			mp4_url: {
				type: 'string',
				default: 'http://'
			},
			
		},
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ), __( 'video', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			var onSelectImage = function( media ) {
				return props.setAttributes( {
					cover_image: media.url,
					cover_image_id: media.id,
				} );
			};
			
			return [
				
				 el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
						el( 'h4', { className: 'clapat-editor-block-title' }, __('Grenada Hosted Video', 'grenada-gutenberg' )),
						
						el( 'div', { className: 'clapat-editor-image' },
							el( MediaUpload, {
								onSelect: onSelectImage,
								type: 'image',
								value: props.attributes.cover_image_id,
								render: function( obj ) {
									return el( components.Button, {
											className: props.attributes.cover_image_id ? 'clapat-image-added' : 'button button-large',
											onClick: obj.open
										},
										! props.attributes.cover_image_id ? i18n.__( 'Upload Video Cover Image', 'grenada-gutenberg' ) : el( 'img', { src: props.attributes.cover_image } ),
										el ('p', {}, __( 'Cover Image', 'grenada-gutenberg' ) )
									);
								}
							} ),
						),
						
						el ('p', { className: 'clapat-editor-label' }, __( 'MP4 video url:', 'grenada-gutenberg' ) ),
						
						el( PlainText,
						{
							value: props.attributes.mp4_url,
							className: 'clapat-inline-content',
							onChange: ( value ) => { props.setAttributes( { mp4_url: value } ); },
						}),
						
						el ('p', { className: 'clapat-editor-label' }, __( 'Webm video url:', 'grenada-gutenberg' ) ),
						
						el( PlainText,
						{
							value: props.attributes.webm_url,
							className: 'clapat-inline-content',
							onChange: ( value ) => { props.setAttributes( { webm_url: value } ); },
						}),
					)
			]
		},
		save: function( props ) {
			
			return '[grenada_video cover_img_url="' + props.attributes.cover_image + '" mp4_url="' + props.attributes.mp4_url + '" webm_url="' + props.attributes.webm_url + '" extra_class_name=""][/grenada_video]'; 
		},
	} );
	

	/** Google Map **/
	blocks.registerBlockType( 'grenada-gutenberg/google-map', {
		title: __( 'Grenada: Google Map', 'grenada-gutenberg' ),
		icon: 'admin-site',
		category: 'layout',
		attributes: {	},
		
		keywords: [ __( 'grenada', 'grenada-gutenberg'), __( 'shortcode', 'grenada-gutenberg' ),  __( 'map', 'grenada-gutenberg' ) ],
		
		edit: function( props ) {
			
			return [
				
				el( 'div', { className: 'clapat-editor-block-wrapper'},  
					
					el( 'h4', { className: 'clapat-editor-block-title' }, __( 'Grenada Google Map', 'grenada-gutenberg' ) ),
					
					el( 'span', { className: 'clapat-inline-caption' },  __( 'Set google map properties in theme options - map.', 'grenada-gutenberg' ) ),
				),
			]
		},
		save: function( props ) {
			
			return '[grenada_map][/grenada_map]'; 
		},
	} );
	
}(
	window.wp.blocks,
	window.wp.blockEditor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components
) );
