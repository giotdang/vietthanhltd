<?php
/**
 * Created by Clapat.
 * Date: 02/10/18
 * Time: 7:26 AM
 */

if ( ! function_exists( 'grenada_load_scripts' ) ){

    function grenada_load_scripts() {

        // Register css files
        wp_register_style( 'fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', TRUE);

        wp_register_style( 'grenada-portfolio', get_template_directory_uri() . '/css/portfolio.css', TRUE);
		
		wp_register_style( 'grenada-packery', get_template_directory_uri() . '/css/packery.css', TRUE);

		wp_register_style( 'grenada-blog', get_template_directory_uri() . '/css/blog.css', TRUE);

		wp_register_style( 'grenada-shortcodes', get_template_directory_uri() . '/css/shortcodes.css', TRUE);

		wp_register_style( 'grenada-assets', get_template_directory_uri() . '/css/assets.css', TRUE);

		// Register scripts
        wp_register_script(
            'modernizr',
            get_template_directory_uri() . '/js/modernizr.js',
            array('jquery'),
            false,
            true
        );
		
		wp_register_script(
            'flexnav',
            get_template_directory_uri() . '/js/jquery.flexnav.min.js',
            array('jquery'),
            false,
            true
        );

		wp_register_script(
            'waitforimages',
            get_template_directory_uri() . '/js/jquery.waitforimages.js',
            array('jquery'),
            false,
            true
        );

		wp_register_script(
            'appear',
            get_template_directory_uri() . '/js/appear.js',
            array('jquery'),
            false,
            true
        );

		wp_register_script(
            'owl-carousel',
            get_template_directory_uri() . '/js/owl.carousel.min.js',
            array('jquery'),
            false,
            true
        );

		wp_register_script(
            'magnific-popup',
            get_template_directory_uri() . '/js/jquery.magnific-popup.min.js',
            array('jquery'),
            false,
            true
        );

		wp_register_script(
            'justified-gallery',
            get_template_directory_uri() . '/js/jquery.justifiedGallery.js',
            array('jquery'),
            false,
            true
        );
		
		wp_register_script(
            'isotope-pkgd',
            get_template_directory_uri() . '/js/isotope.pkgd.js',
            array('jquery'),
            false,
            true
        );
		
		wp_register_script(
            'packery-mode',
            get_template_directory_uri() . '/js/packery-mode.pkgd.js',
            array('jquery'),
            false,
            true
        );
		
		wp_register_script(
            'fit-thumbs',
            get_template_directory_uri() . '/js/fitthumbs.js',
            array('jquery'),
            false,
            true
        );
		
		wp_register_script(
            'justified-gallery',
            get_template_directory_uri() . '/js/jquery.justifiedGallery.js',
            array('jquery'),
            false,
            true
        );

    	wp_register_script(
          'scrollto',
          get_template_directory_uri() . '/js/jquery.scrollto.min.js',
          array('jquery'),
          false,
          true
        );

        wp_register_script(
          'tweenmax',
          get_template_directory_uri() . '/js/tweenmax.min.js',
          array('jquery'),
          false,
          true
        );
		
		wp_register_script(
          'swiper',
          get_template_directory_uri() . '/js/swiper.min.js',
          array('jquery'),
          false,
          true
        );
		
		wp_register_script(
          'scrollmagic',
          get_template_directory_uri() . '/js/scrollmagic.min.js',
          array('jquery'),
          false,
          true
        );
		
		wp_register_script(
          'animation-gsap',
          get_template_directory_uri() . '/js/animation.gsap.min.js',
          array('jquery'),
          false,
          true
        );
		
		wp_register_script(
          'smooth-scrollbar',
          get_template_directory_uri() . '/js/smooth-scrollbar.min.js',
          array('jquery'),
          false,
          true
        );
		
        wp_register_script(
            'grenada-scripts',
            get_template_directory_uri() . '/js/scripts.js',
            array('jquery'),
            false,
            true
        );
		
		wp_enqueue_style('grenada-portfolio');
		wp_enqueue_style('grenada-packery');
        wp_enqueue_style('grenada-blog');
		wp_enqueue_style('grenada-shortcodes');
		wp_enqueue_style('grenada-assets');
		wp_enqueue_style('grenada-theme', get_stylesheet_uri(), array('grenada-portfolio', 'grenada-blog', 'grenada-shortcodes', 'grenada-assets'));
        wp_enqueue_style('fontawesome');
		
		// enqueue standard font style
		$grenada_font_url = '';
		/*
		Translators: If there are characters in your language that are not supported
		by chosen font(s), translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Google font: on or off', 'grenada') ) {
			$grenada_font_url = add_query_arg( 'family', urlencode( 'Poppins:300,400,600,700' ), "//fonts.googleapis.com/css" );
		}
		wp_enqueue_style( 'grenada-font', $grenada_font_url, array(), '1.0.0' );

        // enqueue scripts
        if ( is_singular() ) wp_enqueue_script( 'comment-reply' );

		wp_enqueue_script( 'modernizr' );
		
		wp_enqueue_script( 'flexnav' );

		wp_enqueue_script( 'waitforimages' );

		wp_enqueue_script( 'appear' );

		wp_enqueue_script( 'owl-carousel' );

		wp_enqueue_script( 'magnific-popup' );

		wp_enqueue_script( 'justified-gallery' );
		
		wp_enqueue_script( 'isotope-pkgd' );
		
		wp_enqueue_script( 'packery-mode' );
		
		wp_enqueue_script( 'fit-thumbs' );

		wp_enqueue_script( 'scrollto' );

		wp_enqueue_script( 'tweenmax' );
		
		wp_enqueue_script( 'swiper' );
		
		wp_enqueue_script( 'scrollmagic' );
		
		wp_enqueue_script( 'animation-gsap' );
		
		wp_enqueue_script( 'smooth-scrollbar' );
		
		wp_enqueue_script( 'grenada-scripts' );

		global $grenada_theme_options;
		
		wp_localize_script( 'grenada-scripts',
                    'ClapatGrenadaThemeOptions',
                    array( "enable_ajax" =>  $grenada_theme_options['clapat_grenada_enable_ajax'],
							"enable_preloader"   => $grenada_theme_options['clapat_grenada_enable_preloader'] )
					);

		wp_localize_script( 'grenada-scripts',
							'ClapatMapOptions',
							array(  "map_marker_image"   => esc_js( esc_url ( $grenada_theme_options["clapat_grenada_map_marker"]["url"] ) ),
									"map_address"       			=> $grenada_theme_options['clapat_grenada_map_address'],
									"map_zoom"    				=> $grenada_theme_options['clapat_grenada_map_zoom'],
									"marker_title"  					=> $grenada_theme_options['clapat_grenada_map_company_name'],
									"marker_text"  				=> $grenada_theme_options['clapat_grenada_map_company_info'],
									"map_type" 						=> $grenada_theme_options['clapat_grenada_map_type'],
									"map_api_key"					=> $grenada_theme_options['clapat_grenada_map_api_key'] ) );
    }

}

add_action('wp_enqueue_scripts', 'grenada_load_scripts');
