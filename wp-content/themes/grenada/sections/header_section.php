<?php
// retrieve the path to the logo displayed in the menu bar
$grenada_logo = grenada_get_theme_options( 'clapat_grenada_logo' );
$grenada_logo_path = $grenada_logo['url'];
if( !$grenada_logo_path ){
    $grenada_logo_path = get_template_directory_uri() . "/images/logo.png";
}

$grenada_logo_light = grenada_get_theme_options( 'clapat_grenada_logo_light' );
$grenada_logo_light_path = $grenada_logo_light['url'];
if( !$grenada_logo_light_path ){
    $grenada_logo_light_path = get_template_directory_uri() . "/images/logo-white.png";
}

?>
		<!-- Header -->
        <header class="<?php if( grenada_get_theme_options( 'clapat_grenada_enable_fullscreen_menu' ) ){ echo "fullscreen-menu"; } else { echo "classic-menu"; } ?>">
            <div id="header-container">
            
				<!-- Logo -->
				<div id="logo" class="hide-ball">
					<a class="ajax-link" data-type="page-transition" href="<?php echo esc_url( get_home_url() ); ?>">
						<img class="black-logo" src="<?php echo esc_url( $grenada_logo_path ); ?>" alt="<?php echo esc_attr__('Logo Black', 'grenada'); ?>">
						<img class="white-logo" src="<?php echo esc_url( $grenada_logo_light_path ); ?>" alt="<?php echo esc_attr__('Logo White', 'grenada'); ?>">
					</a>
				</div>
				<!--/Logo -->             
				
				<?php if( is_page_template( 'portfolio-page.php' ) || is_page_template( 'portfolio-mixed-page.php' ) ){ ?>
				<!-- Menu Burger -->
                <div id="show-filters" class="parallax-wrap">
                    <div id="show-filters-wrap" class="parallax-element">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!--/Menu Burger -->
				<?php } ?>
				
				<?php

				get_template_part('sections/menu_section');

				?>
		
				<!-- Menu Burger -->
                <div id="burger-wrapper" class="parallax-wrap">
                    <div id="menu-burger" class="parallax-element">
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!--/Menu Burger -->
        
				<?php if( is_page_template( 'blog-page.php' ) || is_home() || is_archive() || is_search() || is_singular( 'post' ) ){ ?>
				<div id="open-sidebar-nav"><i class="fa fa-arrow-left"></i></div>
				<?php } ?>
            </div>
        </header>
        <!--/Header -->
		
		<?php if( is_page_template( 'blog-page.php' ) || is_home() || is_archive() || is_search() || is_singular( 'post' ) ){ 
			
			// display sidebar section, if defined
			get_template_part('sections/blog_sidebar_section'); 
		} 
		?>
		
		<?php if( is_page_template( 'portfolio-page.php' ) || is_page_template( 'portfolio-mixed-page.php' ) ){
			
			// display sidebar section, if defined
			get_template_part('sections/portfolio_filters_section'); 
		} 
		?>
		<div id="content-scroll">