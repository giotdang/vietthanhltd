<?php
/*
Template name: Portfolio Template
*/
get_header();

if ( have_posts() ){

the_post();

$grenada_hover_effect			= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-page-portfolio-hover-effect' );
$grenada_portfolio_layout	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-page-portfolio-layout' );

$grenada_page_container_list = array();
?>
				
		<!-- Main -->
		<div id="main">
		
		<?php
		
			// display hero section
			get_template_part('sections/hero_section'); 
		
		?>
			<!-- Main Content -->
			<div id="main-content">
				<div id="main-page-content">
				
					<!-- Portfolio -->
					<div id="portfolio-wrap" class="<?php echo sanitize_html_class( $grenada_portfolio_layout ); ?>">
						<div id="portfolio"  class="<?php echo sanitize_html_class( $grenada_hover_effect ); ?> <?php if( grenada_get_theme_options('clapat_grenada_enable_ajax') ){ echo 'thumb-with-ajax'; } else { echo 'thumb-no-ajax'; } ?>">
						<?php

							$grenada_paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							$grenada_args = array(
										'post_type' => 'grenada_portfolio',
										'paged' => $grenada_paged,
										'posts_per_page' => 1000,
										 );

							$grenada_portfolio = new WP_Query( $grenada_args );

							while( $grenada_portfolio->have_posts() ){

								$grenada_portfolio->the_post();

								get_template_part('sections/portfolio_section_item');
								
								$grenada_item_full_image = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-hero-img' );

								if( $grenada_item_full_image && isset( $grenada_item_full_image['url'] ) ){
								
									$grenada_page_container_list[] = $grenada_item_full_image['url'];
								}
								
							}
							
							wp_reset_postdata();
							
							grenada_portfolio_images( $grenada_page_container_list );
						?>
						</div>
					</div>
					<!--/Portfolio -->
					
				</div>
                <!--/Main Page Content -->
			</div>
			<!-- /Main Content -->
		</div>
        <!--/Main -->
<?php
			
}
	
get_footer();

?>