<?php

if ( ! class_exists( 'GrenadaHeroProperties' ) ) {

	class GrenadaHeroProperties
	{
		public $enabled;
		public $caption_title;
		public $caption_subtitle;
		public $position;
		public $opacity;
		public $image;
		public $foreground;
		public $video;
		public $video_webm;
		public $video_mp4;

		public function __construct(){

			$this->enabled = false;
			$this->caption_title = "";
			$this->caption_subtitle = "";
			$this->position = esc_attr("fixed-onscrol");
			$this->image = true;
			$this->foreground= esc_attr('light-content');
			$this->text_alignment = esc_attr("text-align-center");
			$this->video = false;
			$this->video_webm = "";
			$this->video_mp4 = "";
		}

		public function getProperties( $post_type ){

			if( $post_type == 'grenada_portfolio' ){

				$this->enabled 			= true; // in portfolio projects hero is always enabled and the hero image will be displayed in showcase slider
				$this->caption_title	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-hero-caption-title' );
				$this->caption_subtitle = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-hero-caption-subtitle' );
				$this->position 		= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-hero-position' );
				$this->foreground 	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-bknd-color' );
				$this->text_alignment = esc_attr("text-align-center");
				$this->image		 	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-hero-img' );
				$this->video 			= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-video' );
				$this->video_webm 	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-video-webm' );
				$this->video_mp4 	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-video-mp4' );

			} else if( $post_type == 'post' ){

				$this->enabled = true; // the hero section is always enabled in case of blog posts, displaying post title and categories
				$this->caption_title 		= get_the_title();
				$this->caption_subtitle	= grenada_blog_post_hero_caption();
				$this->position 			= esc_attr("parallax-onscroll");
				$this->foreground 		= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-blog-bknd-color' );
				$this->text_alignment 	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-blog-text-alignment' );
				$this->image		 		= null;

			} else if( !empty( $post_type ) ){

				$this->enabled 			= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-page-enable-hero' );
				$this->caption_title	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-page-hero-caption-title' );
				$this->caption_subtitle = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-page-hero-caption-subtitle' );
				$this->position 		= esc_attr("parallax-onscroll");
				$this->foreground 	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-page-bknd-color' );
				$this->text_alignment = grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-page-text-alignment' );
				$this->image		 	= null;

			}
		}

	}
}

$grenada_hero_properties = new GrenadaHeroProperties();

?>
