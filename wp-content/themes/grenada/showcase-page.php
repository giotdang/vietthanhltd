<?php
/*
Template name: Showcase Template
*/

get_header();

if ( have_posts() ){

the_post();

$grenada_showcase_layout	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-page-showcase-layout' );

if( $grenada_showcase_layout == "fullscreen-slider" ){
	
	$grenada_showcase_layout = "split-slider full";
}

$grenada_page_container_list = array();
?>

		<!-- Main -->
		<div id="main">

			<!-- Main Content -->
			<div id="main-content" class="<?php echo esc_attr( $grenada_showcase_layout ); ?>">

				<!-- Showcase Holder -->
                <div id="showcase-holder" class="disabled"> 
					<div id="showcase-slider" class="swiper-container">
						<div class="swiper-wrapper">
							<?php

									$grenada_paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
									$grenada_args = array(
												'post_type' => 'grenada_portfolio',
												'paged' => $grenada_paged,
												'posts_per_page' => 1000,
												 );

									$grenada_portfolio = new WP_Query( $grenada_args );

									$grenada_slides_count = 1;
									while( $grenada_portfolio->have_posts() ){

										$grenada_portfolio->the_post();
										
										$grenada_showcase_include	= grenada_get_post_meta( GRENADA_THEME_OPTIONS, get_the_ID(), 'grenada-opt-portfolio-showcase-include' );
										
										if( $grenada_showcase_include == "yes" ){
										
											$grenada_hero_properties = new GrenadaHeroProperties();
											$grenada_hero_properties->getProperties( get_post_type( get_the_ID() ) );
											
											?>
											<!-- Section Slide -->
											<div class="swiper-slide<?php if( $grenada_slides_count == 1){ echo " active";} ?>" data-number="<?php echo esc_attr( sprintf("%02d", $grenada_slides_count) ); ?>" data-slide="<?php echo esc_attr( $grenada_slides_count ); ?>">
												<a class="ajax-link-project" data-type="page-transition" href="<?php the_permalink(); ?>"> 
													<div class="slide-title" data-number="<?php echo esc_attr( sprintf("%02d", $grenada_slides_count) ); ?>"><span><?php the_title(); ?></span></div>
												</a>
												<div class="slide-caption" data-slide-caption="<?php echo esc_attr( $grenada_slides_count ); ?>"><?php echo wp_kses_post( $grenada_hero_properties->caption_subtitle ); ?></div>  
												
											</div>
											<!--/Section Slide -->
											<?php
											
											$grenada_page_container_list[] = $grenada_hero_properties;
											
											$grenada_slides_count++;
										}

									}

									wp_reset_postdata();
									
									grenada_showcase_slides( $grenada_page_container_list );
							?>
						</div>             
					</div>
                </div>    
                <!-- Showcase Holder -->
				
				<!-- Showcase Gallery -->
                <div id="image-slider">
                    <div class="image-slider-wrapper">                
                        <?php
							$grenada_slides_count = 1;
							foreach( $grenada_page_container_list as $grenada_page_container_item ){
								
								echo '<div class="slider-img" data-slide="' .  esc_attr( $grenada_slides_count ) . '" data-src="' . esc_url( $grenada_page_container_item->image['url'] ) . '">';
								if( $grenada_page_container_item->video ){
									echo '<div class="hero-video-wrapper">';
									echo '<video loop muted class="bgvid">';
									if( !empty( $grenada_page_container_item->video_mp4 ) ){
										echo '<source src="'. esc_url( $grenada_page_container_item->video_mp4 ) .'" type="video/mp4">';
									}
									if( !empty( $grenada_page_container_item->video_webm ) ){
										echo '<source src="'. esc_url( $grenada_page_container_item->video_webm ) .'" type="video/webm">';
									}
									echo '</video>';
									echo '</div>';
								}
								echo '</div>';
								$grenada_slides_count++;
							}
						?>
                    </div>
                </div>
                <!--/Showcase Gallery -->
				
			</div>
			<!-- /Main Content -->

		</div>
        <!--/Main -->

<?php

}

get_footer();

?>
