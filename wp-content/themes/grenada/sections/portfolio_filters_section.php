			<!-- Sidebar Overlay -->
            <div id="filters-overlay">                
                <div id="close-filters"></div>
                <div class="outer">
                    <div class="inner">    
                        <ul id="filters">
                            <li class="filters-timeline link"><a id="all" href="#" data-filter="*" class="active "><?php echo wp_kses_post( grenada_get_theme_options( 'clapat_grenada_portfolio_filter_all_caption' ) ); ?></a></li>
                            <?php

								$grenada_portfolio_category = get_terms('portfolio_category', array( 'hide_empty' => 0 ));

								if( $grenada_portfolio_category ){

									foreach( $grenada_portfolio_category as $portfolio_cat ){

							?>
							<li class="filters-timeline link"><a href="#" data-filter=".<?php echo sanitize_title( $portfolio_cat->slug ); ?>"><?php echo wp_kses_post( $portfolio_cat->name ); ?></a></li>
							<?php

									}
								}

							?>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Sidebar Overlay -->